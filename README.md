# Algoritmo Goertzel


DESCRIPCIÓN
============

El algoritmo Goertzel es un filtro digital derivado de la transformada discreta de Fourier (DFT) que puede detectar las componentes de frecuencia específica en una señal, sin analizar todo el espectro, resultando en un menor tiempo de ejecución.

AUTORES
========

1.Azucena Camacho Del Valle
2.Manuel Santos Gómez
3.Rubén Perea Roldán


LICENCIA
=========

Este proyecto está en el dominio público.



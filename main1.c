
/*****************************************************************************/
/*                                                                           */
/* PROJECT                                                                   */
/*   Goertzel Algorithm                                                      */
/*                                                                           */
/* FILENAME                                                                  */
/*   main1.c                                                                 */
/*                                                                           */
/* DESCRIPTION                                                               */
/*   Test version of the Goertzel Algorithm to check individual single       */
/*   frequencies from keypad using the TMS320C5505 USB Stick.                */
/*                                                                           */
/* REVISION                                                                  */
/*   Revision: 1.01                                                          */
/*   Author  : Richard Sikora                                                */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* HISTORY                                                                   */
/*                                                                           */
/*   Revision 1.00                                                           */
/*   12th April 2010. Modified from Spectrum Digital code.                   */
/*                                                                           */
/*   Revision 1.01.                                                          */
/*   8th August 2010. Modified to be compatible with CSL.                    */
/*                                                                           */
/*****************************************************************************/
/*
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "stdio.h"
#include "usbstk5505.h"
#include "aic3204.h"
#include "PLL.h"
#include "LEDFlasher.h"
#include "stereo.h"
#include "goertzel.h"

Int16 left_input;
Int16 right_input;
Int16 left_output;
Int16 right_output;
Int16 mono_input;


#define SAMPLES_PER_SECOND 8000L
#define GAIN_IN_dB         0

unsigned long int i;

unsigned int step = 0;

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  main( )                                                                 *
 *                                                                          *
 * ------------------------------------------------------------------------ */

void main( void ) 
{

    /* Initialize BSL */
    USBSTK5505_init( );
    
    /* Initialize the Phase Locked Loop in EEPROM */
    pll_frequency_setup(100);

    /* Initialise hardware interface and I2C for code */
    aic3204_hardware_init();
    
    /* Initialise the AIC3204 codec */
    aic3204_init(); 

 
    /* Set sampling frequency in Hz and ADC gain in dB */
    set_sampling_frequency_and_gain(SAMPLES_PER_SECOND, GAIN_IN_dB);

    printf("\nRunning Test Version Goertzel Algorithm Project\n\n");
    printf("Input: Sine wave from signal generator to line input\n");
    printf("Output: Goertzel filter single frequencies on headphones\n\n" );

    puts("1 Flash   = Straight through no processing. Set up signal generator");
    puts("2 Flashes = Filter for 697 Hz. Keys 1 2 3 A");
    puts("3 Flashes = Filter for 770 Hz. Keys 4 5 6 B");
    puts("4 Flashes = Filter for 852 Hz. Keys 7 8 9 C");
    puts("5 Flashes = Filter for 941 Hz. Keys * 0 # D");
    puts("6 Flashes = Filter for 1209 Hz. Keys 1 4 7 *");
    puts("7 Flashes = Filter for 1336 Hz. Keys 2 5 8 0");
    puts("8 Flashes = Filter for 1477 Hz. Keys 3 6 9 #");
    puts("9 Flashes = Filter for 1633 Hz. Keys A B C D");
	
    asm(" bclr XF");
   
    for ( i = 0  ; i < (SAMPLES_PER_SECOND * 600)  ; i++  )
    {

     aic3204_codec_read(&left_input, &right_input); /* Configured for one interrupt per two channels */

     mono_input = stereo_to_mono(left_input, right_input); /* Mono input from microphone */ 
 
     step = LEDFlasher(9); // 9 different Goertzel filter settings.
       
     if ( 1 == step)
       {

        left_output = mono_input; /* Straight through, no processing. Set up microphone */
        right_output = mono_input;
       }
     else if ( 2 == step)
       { 

        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_697_Hz); 
        right_output = left_output;
       }
     else if ( 3 == step)
       {

        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_770_Hz); 
         right_output = left_output;           
       } 
     else if ( 4 == step)
       {

        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_852_Hz); 
        right_output = left_output;           
       }         
     else if ( 5 == step)
       {

        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_941_Hz); 
        right_output = left_output;           
       }               
     else if ( 6 == step)
       {

        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_1209_Hz); 
        right_output = left_output;           
       }  
     else if ( 7 == step)
       {
              
        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_1336_Hz); 
        right_output = left_output;           
       }            
     else if ( 8 == step)
       {
             
        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_1477_Hz); 
        right_output = left_output;           
       }  
     else if ( 9 == step)
       {
              
        left_output = goertzel_single_frequency( mono_input, COEFFICIENT_1633_Hz); 
        right_output = left_output;           
       }
 
     aic3204_codec_write(left_output, right_output);
    }

   /* Disable I2S and put codec into reset */ 
    aic3204_disable();

    printf( "\n***Program has Terminated***\n" );
    SW_BREAKPOINT;
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  End of main.c                                                           *
 *                                                                          *
 * ------------------------------------------------------------------------ */












